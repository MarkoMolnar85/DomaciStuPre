﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLayer
{
    public class StudentBusiness
    {
        private StudentRepository studentRepository;
        public StudentBusiness()
        {
            this.studentRepository = new StudentRepository();
        }
        public List<Student> GetAllStudents()
        {
            List<Student> Lst = this.studentRepository.GetAllStudents();
            return Lst;
        }

        public List<Student> GetOlderStudents()
        {
            List<Student> Lst = this.studentRepository.GetAllStudents();
            List<Student> Olders = new List<Student>();
            if (Lst.Count > 0)
            {
                Olders = Lst.Where(s => s.Age > 26).ToList();
            }
            return Olders;
        }
    }
}

﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLayer
{
    public class BusinessStudentSubject
    {
        private StudentSubjectRepository studentSubjectRepository;
        public BusinessStudentSubject()
        {
            this.studentSubjectRepository = new StudentSubjectRepository();
        }
        public List<StudentPredmeti> GetAllStudentsAndSubjects()
        {
            List<StudentPredmeti> Lst = this.studentSubjectRepository.GetAllStudentsTheySubjects();
            return Lst;
        }

        public List<StudentPredmeti> GetSubjectWhitIndex(string s)
        {
            List<StudentPredmeti> ListToReturn = new List<StudentPredmeti>();
            List<StudentPredmeti> ListToTake = this.studentSubjectRepository.GetAllStudentsTheySubjects();
            //ListToReturn = ListToTake.Where(s => s.Index.Equals(index) && s.IndexSubject.Equals(index)).ToList();
            for (int i = 0; i < ListToTake.Count; i++)
            {
                /*string pro = ListToTake[i].Index;
                string pro2 = "";
                for (int j = 0; j < pro.Length; j++)
                {
                    if (pro[j].Equals(" "))
                    { }
                    else
                        pro2 += pro[j];
                }*/
                if (s + "  " == ListToTake[i].Index)
                {
                    ListToReturn.Add(ListToTake[i]);
                }
            }

            return ListToReturn;
            //return ListToTake;
        }
    }
}

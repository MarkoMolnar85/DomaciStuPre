﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLayer
{
    public class SubjectBusiness
    {
        private SubjectRepository subjectRepository;
        public SubjectBusiness()
        {
            this.subjectRepository = new SubjectRepository();
        }
        public List<Subject> GetAllSubjects()
        {
            List<Subject> Lst = this.subjectRepository.GetAllSubjects();
            return Lst;
        }

    }
}

﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class StudentRepository
    {
        private string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

        public List<Student> GetAllStudents()
        {
            List<Student> ListToReturn = new List<Student>();
            using (SqlConnection dataConnection = new SqlConnection(this.connectionString))
            {
                dataConnection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = dataConnection;
                cmd.CommandText = "select * from students";
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Student s = new Student();
                    s.Id = reader.GetInt32(0);
                    s.Name = reader.GetString(1);
                    s.Surname = reader.GetString(2);
                    s.Age = reader.GetInt32(3);
                    s.IndexNumber = reader.GetString(4);
                    ListToReturn.Add(s);
                }
                return ListToReturn;
            }
        }
    }
}

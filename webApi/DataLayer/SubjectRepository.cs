﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class SubjectRepository
    {
        private string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

        public List<Subject> GetAllSubjects()
        {
            List<Subject> ListToReturn = new List<Subject>();
            using (SqlConnection dataConnection = new SqlConnection(this.connectionString))
            {
                dataConnection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = dataConnection;
                cmd.CommandText = "select * from subjects";
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Subject s = new Subject();
                    s.Id = reader.GetInt32(0);
                    s.SubjectName = reader.GetString(1);
                    s.IndexNumber = reader.GetString(2);
                    ListToReturn.Add(s);
                }
                return ListToReturn;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class StudentPredmeti
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Index { get; set; }
        public string IndexSubject { get; set; }
        public string Subject { get; set; }
    }
}

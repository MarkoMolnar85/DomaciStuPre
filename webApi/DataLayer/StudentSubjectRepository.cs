﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class StudentSubjectRepository
    {
        private string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

        public List<StudentPredmeti> GetAllStudentsTheySubjects()
        {
            List<StudentPredmeti> ListToReturn = new List<StudentPredmeti>();
            using (SqlConnection dataConnection = new SqlConnection(this.connectionString))
            {
                dataConnection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = dataConnection;
                cmd.CommandText = "select students.Name, students.Surname, students.IndexNumber, Subjects.SubjectName, Subjects.IndexNumber from students, subjects where students.IndexNumber = subjects.IndexNumber ";
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    StudentPredmeti s = new StudentPredmeti();
                    s.Name = reader.GetString(0);
                    s.Surname = reader.GetString(1);
                    s.Index = reader.GetString(2);
                    s.Subject = reader.GetString(3);
                    s.IndexSubject = reader.GetString(4);
                    
                    ListToReturn.Add(s);

                }
                return ListToReturn;

            }
        }
    }
}

﻿using BussinesLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace webApi.Controllers
{
    [RoutePrefix("api/past")]
    public class StudentSubjectController : ApiController
    {
        BusinessStudentSubject bs;
        public StudentSubjectController()
        {
            this.bs = new BusinessStudentSubject();
        }
        // GET: api/StudentSubject
        [HttpGet]
        [Route("getall")]
        public List<StudentPredmeti> GetAllPast()
        {
            return bs.GetAllStudentsAndSubjects();
        }

        // GET: api/StudentSubject/5
        [HttpGet]
        [Route("whitindex")]
        public List<StudentPredmeti> getAllWhitIndex(string s)
        //public string getAllWhitIndex(string s)
        {
            return bs.GetSubjectWhitIndex(s);
            //return s;
        }

        // POST: api/StudentSubject
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/StudentSubject/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/StudentSubject/5
        public void Delete(int id)
        {
        }
    }
}

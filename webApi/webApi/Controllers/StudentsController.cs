﻿using BussinesLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace webApi.Controllers
{
    [RoutePrefix("api/student")]
    public class StudentsController : ApiController
    {
        private StudentBusiness sbus;
        public StudentsController()
        {
            sbus = new StudentBusiness();
        }
        [Route("getall")]
        [HttpGet]
        public List<Student> GetAllStudents()
        {
            return sbus.GetAllStudents();
        }

        // GET: api/Students/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Students
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Students/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Students/5
        public void Delete(int id)
        {
        }
    }
}

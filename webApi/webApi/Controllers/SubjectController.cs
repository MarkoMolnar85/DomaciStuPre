﻿using BussinesLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace webApi.Controllers
{
    [RoutePrefix("api/subject")]
    public class SubjectController : ApiController
    {
        private SubjectBusiness sb;
        public SubjectController()
        {
            sb = new SubjectBusiness();
        }
        // GET: api/Subject
        [HttpGet]
        [Route("getsubjects")]
        public List<Subject> Get()
        {
            return sb.GetAllSubjects();
        }

        // GET: api/Subject/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Subject
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Subject/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Subject/5
        public void Delete(int id)
        {
        }
    }
}
